package com.epam.model;


public interface Model {

     void parseJackson();

     void parseGson();

     void sortJson();

    void validateJson();
}
