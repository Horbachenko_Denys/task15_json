package com.epam.model;

public class Account {

    private int id;
    private String type;
    private int amount;
    private double probability;
    private int timeConstraints;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public int getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(int timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public String toString() {
        return " Id: " +  this.id + ", type: " +  this.type + ", amount: " +  this.amount + "$" +
                ", probability: " +  this.probability + ", time: " +  this.timeConstraints + " years";
    }
}
