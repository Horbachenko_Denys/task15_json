package com.epam.model;

import com.epam.model.parser.BankComparator;
import com.epam.model.parser.GsonParser;
import com.epam.model.parser.JacksonParser;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ModelImpl implements Model {
    private String jsonPath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task15_json\\src\\main\\resources\\json\\banks.json";
    private String jsonSchemaPath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task15_json\\src\\main\\resources\\json\\banksSchemaMy.json";
    private JacksonParser jacksonParser = new JacksonParser();
    @Override
    public void parseJackson() {
        try {
            System.out.println("=====Jackson Parser=====");
            Arrays.asList(jacksonParser.parseBanks(jsonPath))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseGson() {
        try {
            GsonParser gsonParser = new GsonParser();
            System.out.println("=====Gson Parser=====");
            Arrays.asList(gsonParser.parseBanks(jsonPath))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortJson() {
        try {
            List<Bank> bankList = Arrays.asList(jacksonParser.parseBanks(jsonPath));
            bankList.sort(new BankComparator());
            bankList.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateJson() {

        try {
            System.out.println(JsonValidator.isValid(jsonPath, jsonSchemaPath));
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }
}