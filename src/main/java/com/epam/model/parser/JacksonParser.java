package com.epam.model.parser;

import com.epam.model.Bank;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JacksonParser {
    private ObjectMapper objectMapper = new ObjectMapper();

    public Bank[] parseBanks(String jsonFilePath) throws IOException {
        return objectMapper.readValue(new File(jsonFilePath), Bank[].class);
    }
}
