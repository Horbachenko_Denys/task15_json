package com.epam.model.parser;

import com.epam.model.Account;
import com.epam.model.Bank;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {

    @Override
    public int compare(Bank b1, Bank b2) {
        return Character.compare(b1.getName().toCharArray()[0], b2.getName().toCharArray()[0]);
    }
}
