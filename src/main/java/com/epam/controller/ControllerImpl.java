package com.epam.controller;


import com.epam.model.Model;
import com.epam.model.ModelImpl;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }


    @Override
    public void parseJackson() {
        model.parseJackson();
    }

    @Override
    public void parseGson() {
        model.parseGson();
    }

    @Override
    public void sortJson() {
        model.sortJson();
    }

    @Override
    public void validateJson() {
        model.validateJson();
    }
}