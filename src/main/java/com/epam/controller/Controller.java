package com.epam.controller;

public interface Controller {

    void parseJackson();

    void parseGson();

    void sortJson();

    void validateJson();
}


